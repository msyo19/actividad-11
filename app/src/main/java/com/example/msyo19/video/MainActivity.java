package com.example.msyo19.video;

import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.MediaController;
import android.widget.VideoView;

public class MainActivity extends AppCompatActivity {
    private VideoView miVideoView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        miVideoView=findViewById(R.id.vviwer);
        MediaController controller = new MediaController(this);
        controller.setMediaPlayer(miVideoView);

    }
    private void initializerPlayer(){
        miVideoView.setVideoPath("https://www.ebookfrenzy.com/android_book/movie.mp4");
        miVideoView.start();
    }
    private void releasePlayer(){miVideoView.stopPlayback();}

    protected void onStart(){
        super.onStart();
        initializerPlayer();
    }

    protected void onStop(){
        super.onStop();
        releasePlayer();
    }

    protected void onPause(){
        super.onPause();
        if (Build.VERSION.SDK_INT< Build.VERSION_CODES.N){
            miVideoView.pause();
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        miVideoView.start();
    }
}
